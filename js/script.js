function openName(elm, name) {
  let descr = document.getElementsByClassName("descr");
  for (let i = 0; i < descr.length; i++) {
    descr[i].style.display = "none"
  };
  let nametitle = document.getElementsByClassName("nametitle");
  for (i = 0; i < nametitle.length; i++) {
    nametitle[i].className = nametitle[i].className.replace(" nametitle active", "")
  };
  document.getElementById(name).style.display = "block";
  elm.currentTarget.className += " nametitle active";
}

function show() {
  document.getElementById('hide').style.display = "block";
  document.getElementById('btnID').style.display = 'none'
}

let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  let dots = document.getElementsByClassName("demo");
  let captionText = document.getElementById("caption");
  if (n > slides.length) { slideIndex = 1 }
  if (n < 1) { slideIndex = slides.length }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none"}
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "")}
  slides[slideIndex - 1].style.display = "block";
  dots[slideIndex - 1].className += " active";
  captionText.innerHTML = dots[slideIndex - 1].alt;}

filterSelection("all")
function filterSelection(c) {
  let x, i;
  x = document.getElementsByClassName("content");
  if (c == "all") c = "";
  for (i = 0; i < x.length; i++) {
    RemoveClass(x[i], "show");
    if (x[i].className.indexOf(c) > -1) AddClass(x[i], "show")}}

function AddClass(element, name) {
  let i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {
      element.className += " " + arr2[i];
    }}}

function RemoveClass(element, name) {
  let i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);
    }}
  element.className = arr1.join(" ")};

let btnContainer = document.getElementById("myBtnContainer");
let btns = btnContainer.getElementsByClassName("btn");
for (let i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function () {
    let current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  })}